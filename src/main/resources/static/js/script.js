/**
 * 应用基础地址，用于处理动态生成的元素的地址问题
 * @type {string}
 */
var baseUrl = '';

$(function () {
    baseUrl = $('#baseUrl').attr('href');
});