package com.liusongpo.util;

import tk.mybatis.mapper.genid.GenId;

import java.util.UUID;

/**
 * @author 刘松坡
 */
public class UUIDGenId implements GenId<String> {
    @Override
    public String genId(String table, String column) {
        return UUID.randomUUID().toString();
    }
}
