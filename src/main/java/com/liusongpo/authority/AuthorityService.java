package com.liusongpo.authority;

import com.liusongpo.common.SimpleServiceImpl;
import com.liusongpo.entity.AuthorityInfo;
import com.liusongpo.entity.AuthorityResource;
import com.liusongpo.entity.ResourceInfo;
import com.liusongpo.mapper.AuthorityInfoMapper;
import com.liusongpo.mapper.AuthorityResourceMapper;
import com.liusongpo.mapper.FakeAuthorityMapper;
import com.liusongpo.mapper.ResourceInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityService extends SimpleServiceImpl<AuthorityInfo, String> {

    private final FakeAuthorityMapper fakeAuthorityMapper;

    private final ResourceInfoMapper resourceInfoMapper;

    private final AuthorityResourceMapper authorityResourceMapper;

    @Autowired
    public AuthorityService(AuthorityInfoMapper mapper, FakeAuthorityMapper fakeAuthorityMapper, ResourceInfoMapper resourceInfoMapper, AuthorityResourceMapper authorityResourceMapper) {
        super(mapper);
        this.fakeAuthorityMapper = fakeAuthorityMapper;
        this.resourceInfoMapper = resourceInfoMapper;
        this.authorityResourceMapper = authorityResourceMapper;
    }

    public AuthorityInfo selectById(String id) {
        return this.fakeAuthorityMapper.selectAuthorityById(id);
    }

    public List<ResourceInfo> selectResourceList() {
        return resourceInfoMapper.selectAll();
    }

    /**
     * 添加权限信息
     *
     * @param authorityInfo 权限信息
     * @param resources 资源标识
     */
    public void save(AuthorityInfo authorityInfo, String resources) {
        this.mapper.insertSelective(authorityInfo);
        for (String resourceId : resources.split(",")) {
            this.authorityResourceMapper.insertSelective(new AuthorityResource(){{
                setAuthorityId(authorityInfo.getId());
                setResourceId(resourceId);
            }});
        }
    }

    /**
     * 更新权限信息
     *
     * @param authorityInfo 权限信息
     * @param resources 资源标识
     */
    public void update(AuthorityInfo authorityInfo, String resources) {
        // 更新
        this.mapper.updateByPrimaryKeySelective(authorityInfo);
        // 删除关系
        this.authorityResourceMapper.delete(new AuthorityResource(){{
            setAuthorityId(authorityInfo.getId());
        }});

        // 添加关系
        for (String resourceId : resources.split(",")) {
            this.authorityResourceMapper.insertSelective(new AuthorityResource(){{
                setAuthorityId(authorityInfo.getId());
                setResourceId(resourceId);
            }});
        }
    }
}
