package com.liusongpo.authority;

import com.github.pagehelper.PageInfo;
import com.liusongpo.entity.AuthorityInfo;
import com.liusongpo.entity.ResourceInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Controller
@RequestMapping("/authorities")
public class AuthorityController {

    private final AuthorityService authorityService;

    @Autowired
    public AuthorityController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @GetMapping("/add")
    public ModelAndView add() {
        ModelAndView mav = new ModelAndView("views/authority/add");
        List<ResourceInfo> resourceInfoList = this.authorityService.selectResourceList();
        mav.addObject("resourceList", resourceInfoList);
        return mav;
    }

    @PostMapping("/save")
    public RedirectView save(AuthorityInfo authorityInfo, String resources) {
        this.authorityService.save(authorityInfo, resources);
        return new RedirectView("/authorities/page");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable String id) {
        ModelAndView mav = new ModelAndView("views/authority/edit");
        List<ResourceInfo> resourceInfoList = this.authorityService.selectResourceList();
        mav.addObject("resourceList", resourceInfoList);
        mav.addObject("item", this.authorityService.selectById(id));
        return mav;
    }

    @PostMapping("/update")
    public RedirectView update(AuthorityInfo authorityInfo, String resources) {
        this.authorityService.update(authorityInfo, resources);
        return new RedirectView("/authorities/page");
    }

    @GetMapping("/delete/{id}")
    public RedirectView delete(@PathVariable String id) {
        this.authorityService.deleteByPrimaryKey(id);
        return new RedirectView("/authorities/page");
    }

    @GetMapping("/info/{id}")
    @ResponseBody
    public AuthorityInfo info(@PathVariable String id) {
        return this.authorityService.selectById(id);
    }

    @GetMapping("/page")
    public ModelAndView page(String authorityname, Integer page, Integer size) {
        ModelAndView mav = new ModelAndView("views/authority/list");
        PageInfo<AuthorityInfo> pageInfo;
        Example example = null;
        if (StringUtils.isNotBlank(authorityname)) {
            example = new Example(AuthorityInfo.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andLike("name", "%" + authorityname + "%");
        }
        pageInfo = this.authorityService.selectPageByExample(example, page, size);
        mav.addObject("pageInfo", pageInfo);
        mav.addObject("name", authorityname);
        return mav;
    }
}
