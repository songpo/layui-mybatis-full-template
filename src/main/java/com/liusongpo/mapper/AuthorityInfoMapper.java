package com.liusongpo.mapper;

import com.liusongpo.entity.AuthorityInfo;
import tk.mybatis.mapper.common.Mapper;

public interface AuthorityInfoMapper extends Mapper<AuthorityInfo> {
}