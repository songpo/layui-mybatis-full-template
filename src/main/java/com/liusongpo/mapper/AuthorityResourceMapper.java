package com.liusongpo.mapper;

import com.liusongpo.entity.AuthorityResource;
import tk.mybatis.mapper.common.Mapper;

public interface AuthorityResourceMapper extends Mapper<AuthorityResource> {
}