package com.liusongpo.mapper;

import com.liusongpo.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
}