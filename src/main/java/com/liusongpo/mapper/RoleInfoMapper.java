package com.liusongpo.mapper;

import com.liusongpo.entity.RoleInfo;
import tk.mybatis.mapper.common.Mapper;

public interface RoleInfoMapper extends Mapper<RoleInfo> {
}