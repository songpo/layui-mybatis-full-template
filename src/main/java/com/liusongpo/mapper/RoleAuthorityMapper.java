package com.liusongpo.mapper;

import com.liusongpo.entity.RoleAuthority;
import tk.mybatis.mapper.common.Mapper;

public interface RoleAuthorityMapper extends Mapper<RoleAuthority> {
}