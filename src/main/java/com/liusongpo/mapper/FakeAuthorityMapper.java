package com.liusongpo.mapper;

import com.liusongpo.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FakeAuthorityMapper {

    /**
     * 根据账号查询用户信息
     *
     * @param username 账号
     * @return 用户信息
     */
    UserInfo selectByUsername(String username);

    /**
     * 根据主键查询用户信息
     * @param id 用户标识
     * @return 用户信息
     */
    UserInfo selectUserById(String id);

    /**
     * 根据主键查询角色信息
     * @param id 角色标识
     * @return 角色信息
     */
    RoleInfo selectRoleById(String id);

    /**
     * 根据用户标识查询角色列表
     *
     * @param userId 用户标识
     * @return 角色列表
     */
    List<RoleInfo> selectRoleListByUserId(String userId);

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<RoleInfo> selectRoleList();

    /**
     * 查询资源所属角色列表
     *
     * @param resourceId 资源标识
     * @return 角色列表
     */
    List<RoleInfo> selectRoleListByResourceId(String resourceId);

    /**
     * 根据主键查询权限信息
     * @param id 权限标识
     * @return 权限信息
     */
    AuthorityInfo selectAuthorityById(String id);

    /**
     * 查询权限列表
     *
     * @return 权限列表
     */
    List<AuthorityInfo> selectAuthorityList();

    /**
     * 查询权限列表
     *
     * @param roleId 角色标识
     * @return 权限列表
     */
    List<AuthorityInfo> selectAuthorityListByRoleId(String roleId);

    /**
     * 查询资源列表
     *
     * @return 资源列表
     */
    List<ResourceInfo> selectResourceList();

    /**
     * 查询资源列表
     *
     * @param authorityId 权限标识
     * @return 资源列表
     */
    List<ResourceInfo> selectResourceListByAuthorityId(String authorityId);

    /**
     * 查询用户角色列表
     *
     * @param userId 用户标识
     * @return 用户角色列表
     */
    List<UserRole> selectUserRoleListByUserId(String userId);

    /**
     * 查询用户角色列表
     *
     * @param userIds 用户标识数组
     * @return 用户角色列表
     */
    List<UserRole> selectUserRoleListByUserIds(@Param("userIds") List<String> userIds);

    /**
     * 查询用户列表-带角色
     *
     * @param username 账号
     * @return 用户列表
     */
    List<UserInfo> selectUserRoleList(String username);

    /**
     * 查询角色列表-带权限
     *
     * @param name 角色名称
     * @return 角色列表
     */
    List<RoleInfo> selectRoleAuthorityList(String name);

}
