package com.liusongpo.mapper;

import com.liusongpo.entity.ResourceInfo;
import tk.mybatis.mapper.common.Mapper;

public interface ResourceInfoMapper extends Mapper<ResourceInfo> {
}