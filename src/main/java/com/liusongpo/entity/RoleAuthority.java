package com.liusongpo.entity;

import com.liusongpo.util.UUIDGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "`role_authority`")
public class RoleAuthority {
    /**
     * 唯一标识
     */
    @Id
    @Column(name = "`id`")
    @KeySql(genId = UUIDGenId.class)
    private String id;

    /**
     * 角色标识
     */
    @Column(name = "`role_id`")
    private String roleId;

    @Transient
    private RoleInfo role;

    /**
     * 权限标识
     */
    @Column(name = "`authority_id`")
    private String authorityId;

    @Transient
    private AuthorityInfo authority;

    /**
     * 获取唯一标识
     *
     * @return id - 唯一标识
     */
    public String getId() {
        return id;
    }

    /**
     * 设置唯一标识
     *
     * @param id 唯一标识
     */
    public void setId(String id) {
        this.id = id;
    }

    public RoleInfo getRole() {
        return role;
    }

    public void setRole(RoleInfo role) {
        this.role = role;
    }

    public AuthorityInfo getAuthority() {
        return authority;
    }

    public void setAuthority(AuthorityInfo authority) {
        this.authority = authority;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }
}