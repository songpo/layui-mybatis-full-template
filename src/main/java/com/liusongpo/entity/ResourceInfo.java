package com.liusongpo.entity;

import com.liusongpo.util.UUIDGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "`resource_info`")
public class ResourceInfo {
    /**
     * 唯一标识
     */
    @Id
    @Column(name = "`id`")
    @KeySql(genId = UUIDGenId.class)
    private String id;

    /**
     * 名称
     */
    @Column(name = "`name`")
    private String name;

    /**
     * 内容
     */
    @Column(name = "`content`")
    private String content;

    /**
     * 获取唯一标识
     *
     * @return id - 唯一标识
     */
    public String getId() {
        return id;
    }

    /**
     * 设置唯一标识
     *
     * @param id 唯一标识
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }
}