package com.liusongpo.entity;

import com.liusongpo.util.UUIDGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "`authority_resource`")
public class AuthorityResource {
    /**
     * 唯一标识
     */
    @Id
    @Column(name = "`id`")
    @KeySql(genId = UUIDGenId.class)
    private String id;

    /**
     * 权限标识
     */
    @Column(name = "`authority_id`")
    private String authorityId;

    @Transient
    private AuthorityInfo authority;

    /**
     * 资源标识
     */
    @Column(name = "`resource_id`")
    private String resourceId;

    @Transient
    private ResourceInfo resource;

    /**
     * 获取唯一标识
     *
     * @return id - 唯一标识
     */
    public String getId() {
        return id;
    }

    /**
     * 设置唯一标识
     *
     * @param id 唯一标识
     */
    public void setId(String id) {
        this.id = id;
    }

    public AuthorityInfo getAuthority() {
        return authority;
    }

    public void setAuthority(AuthorityInfo authority) {
        this.authority = authority;
    }

    public ResourceInfo getResource() {
        return resource;
    }

    public void setResource(ResourceInfo resource) {
        this.resource = resource;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }
}