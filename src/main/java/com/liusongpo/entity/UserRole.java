package com.liusongpo.entity;

import com.liusongpo.util.UUIDGenId;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "`user_role`")
public class UserRole {
    /**
     * 唯一标识
     */
    @Id
    @Column(name = "`id`")
    @KeySql(genId = UUIDGenId.class)
    private String id;

    /**
     * 用户标识
     */
    @Column(name = "`user_id`")
    private String userId;

    @Transient
    private UserInfo user;

    /**
     * 角色标识
     */
    @Column(name = "`role_id`")
    private String roleId;

    @Transient
    private RoleInfo role;

    /**
     * 获取唯一标识
     *
     * @return id - 唯一标识
     */
    public String getId() {
        return id;
    }

    /**
     * 设置唯一标识
     *
     * @param id 唯一标识
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public RoleInfo getRole() {
        return role;
    }

    public void setRole(RoleInfo role) {
        this.role = role;
    }
}