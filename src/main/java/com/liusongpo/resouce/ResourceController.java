package com.liusongpo.resouce;

import com.github.pagehelper.PageInfo;
import com.liusongpo.entity.ResourceInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import tk.mybatis.mapper.entity.Example;

@Controller
@RequestMapping("/resources")
public class ResourceController {

    private final ResourceService resourceService;

    @Autowired
    public ResourceController(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @GetMapping("/add")
    public ModelAndView add() {
        return new ModelAndView("views/resource/add");
    }

    @PostMapping("/save")
    public RedirectView save(ResourceInfo resourceInfo) {
        this.resourceService.insertSelective(resourceInfo);
        return new RedirectView("/resources/page");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable String id) {
        ModelAndView mav = new ModelAndView("views/resource/edit");
        mav.addObject("item", this.resourceService.selectByPrimaryKey(id));
        return mav;
    }

    @PostMapping("/update")
    public RedirectView update(ResourceInfo resourceInfo) {
        this.resourceService.updateByPrimaryKeySelective(resourceInfo);
        return new RedirectView("/resources/page");
    }

    @GetMapping("/delete/{id}")
    public RedirectView delete(@PathVariable String id) {
        this.resourceService.deleteByPrimaryKey(id);
        return new RedirectView("/resources/page");
    }

    @GetMapping("/page")
    public ModelAndView page(String name, Integer page, Integer size) {
        ModelAndView mav = new ModelAndView("views/resource/list");
        PageInfo<ResourceInfo> pageInfo;
        Example example = null;
        if (StringUtils.isNotBlank(name)) {
            example = new Example(ResourceInfo.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andLike("name", "%" + name + "%");
        }
        pageInfo = this.resourceService.selectPageByExample(example, page, size);
        mav.addObject("pageInfo", pageInfo);
        mav.addObject("name", name);
        return mav;
    }
}
