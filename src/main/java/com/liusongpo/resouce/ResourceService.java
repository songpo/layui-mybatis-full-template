package com.liusongpo.resouce;

import com.liusongpo.common.SimpleServiceImpl;
import com.liusongpo.entity.ResourceInfo;
import com.liusongpo.mapper.ResourceInfoMapper;
import org.springframework.stereotype.Service;

@Service
public class ResourceService extends SimpleServiceImpl<ResourceInfo, String> {

    public ResourceService(ResourceInfoMapper mapper) {
        super(mapper);
    }

}
