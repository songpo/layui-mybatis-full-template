package com.liusongpo.system;

import com.liusongpo.entity.UserInfo;
import com.liusongpo.security.CurrentUser;
import com.liusongpo.security.LoginUser;
import com.liusongpo.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * @author 刘松坡
 */
@Controller
public class CommonController {

    private final BCryptPasswordEncoder passwordEncoder;

    private final UserService userService;

    @Autowired
    public CommonController(BCryptPasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @GetMapping(value = { "/", "/home" })
    public ModelAndView home(@CurrentUser LoginUser loginUser) {
        ModelAndView mav = new ModelAndView("views/login");
        if (null != loginUser) {
            mav.setViewName("views/home");
        }
        return mav;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("views/login");
    }

    @GetMapping("/register")
    public ModelAndView register() {
        return new ModelAndView("views/register");
    }

    @PostMapping("/register")
    public RedirectView register(String username, String password) {
        String viewName = "/register";
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
            UserInfo userInfo = this.userService.selectOne(new UserInfo(){{
                setUsername(username);
            }});
            if (null != userInfo) {
                viewName += "?error=账号已存在";
            } else {
                userInfo = new UserInfo();
                userInfo.setUsername(username);
                userInfo.setPassword(this.passwordEncoder.encode(password));
                this.userService.insertSelective(userInfo);
                viewName = "/home";
            }
        } else {
            viewName += "?error=账号或密码为空";
        }
        return new RedirectView(viewName);
    }

    @GetMapping("/access-denied")
    public ModelAndView accessDenied() {
        return new ModelAndView("views/403");
    }
}
