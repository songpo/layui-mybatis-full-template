package com.liusongpo.user;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liusongpo.common.SimpleServiceImpl;
import com.liusongpo.entity.RoleInfo;
import com.liusongpo.entity.UserInfo;
import com.liusongpo.entity.UserRole;
import com.liusongpo.mapper.FakeAuthorityMapper;
import com.liusongpo.mapper.RoleInfoMapper;
import com.liusongpo.mapper.UserInfoMapper;
import com.liusongpo.mapper.UserRoleMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class UserService extends SimpleServiceImpl<UserInfo, String> {

    private final FakeAuthorityMapper fakeAuthorityMapper;

    private final UserRoleMapper userRoleMapper;

    private final RoleInfoMapper roleInfoMapper;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserInfoMapper mapper, FakeAuthorityMapper fakeAuthorityMapper, UserRoleMapper userRoleMapper, BCryptPasswordEncoder passwordEncoder, RoleInfoMapper roleInfoMapper) {
        super(mapper);
        this.fakeAuthorityMapper = fakeAuthorityMapper;
        this.userRoleMapper = userRoleMapper;
        this.passwordEncoder = passwordEncoder;
        this.roleInfoMapper = roleInfoMapper;
    }

    public void save(UserInfo userInfo, String roles) {
        // 加密密码
        userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        // 设置为启用
        userInfo.setEnabled(true);

        this.mapper.insertSelective(userInfo);
        // 添加关系
        for (String roleId : roles.split(",")) {
            this.userRoleMapper.insertSelective(new UserRole(){{
                setUserId(userInfo.getId());
                setRoleId(roleId);
            }});
        }
    }

    public void update(UserInfo userInfo, String roles) {
        this.mapper.updateByPrimaryKeySelective(userInfo);

        // 删除之前的关系
        this.userRoleMapper.delete(new UserRole(){{
            setUserId(userInfo.getId());
        }});

        // 添加关系
        for (String roleId : roles.split(",")) {
            this.userRoleMapper.insertSelective(new UserRole(){{
                setUserId(userInfo.getId());
                setRoleId(roleId);
            }});
        }
    }

    public List<RoleInfo> selectRoleList() {
        return this.roleInfoMapper.selectAll();
    }

    public UserInfo selectById(String id) {
        return this.fakeAuthorityMapper.selectUserById(id);
    }

    public PageInfo<UserInfo> selectPage(String username, Integer page, Integer size) {
        Example example = new Example(UserInfo.class);
        if (StringUtils.isNotBlank(username)) {
            example.createCriteria().andLike("username", "%" + username + "%");
        }
        if (null == page) {
            page = 1;
        }
        if (null == size) {
            size = 10;
        }
        PageHelper.startPage(page, size);
        List<UserInfo> userInfoList = this.mapper.selectByExample(example);
        return new PageInfo<>(userInfoList);
    }

}
