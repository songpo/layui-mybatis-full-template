package com.liusongpo.user;

import com.github.pagehelper.PageInfo;
import com.liusongpo.entity.RoleInfo;
import com.liusongpo.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/add")
    public ModelAndView add() {
        ModelAndView mav = new ModelAndView("views/user/add");
        List<RoleInfo> roleList = this.userService.selectRoleList();
        mav.addObject("roleList", roleList);
        return mav;
    }

    @PostMapping("/save")
    public RedirectView save(UserInfo userInfo, String roles) {
        this.userService.save(userInfo, roles);
        return new RedirectView("/users/page");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable String id) {
        ModelAndView mav = new ModelAndView("views/user/edit");

        List<RoleInfo> roleList = this.userService.selectRoleList();
        mav.addObject("roleList", roleList);

        mav.addObject("item", this.userService.selectById(id));
        return mav;
    }

    @PostMapping("/update")
    public RedirectView update(UserInfo userInfo, String roles) {
        this.userService.update(userInfo, roles);
        return new RedirectView("/users/page");
    }

    @GetMapping("/delete/{id}")
    public RedirectView delete(@PathVariable String id) {
        this.userService.deleteByPrimaryKey(id);
        return new RedirectView("/users/page");
    }

    @GetMapping("/info/{id}")
    @ResponseBody
    public UserInfo info(@PathVariable String id) {
        return this.userService.selectById(id);
    }

    @PostMapping("/status")
    public RedirectView updateStatus(UserInfo userInfo) {
        this.userService.updateByPrimaryKeySelective(userInfo);
        return new RedirectView("/users/page");
    }

    @GetMapping("/page")
    public ModelAndView page(String username, Integer page, Integer size) {
        ModelAndView mav = new ModelAndView("views/user/list");
        PageInfo<UserInfo> pageInfo = this.userService.selectPage(username, page, size);
        mav.addObject("pageInfo", pageInfo);
        mav.addObject("username", username);
        return mav;
    }
}
