package com.liusongpo.common;

import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IBaseService<T, PK> {

    /**
     * 添加
     * @param model 实体
     * @return 1 成功 | 0 失败
     */
    int insert(T model);

    /**
     * 添加赋值字段
     * @param model 实体
     * @return 1 成功 | 0 失败
     */
    int insertSelective(T model);

    /**
     * 删除
     * @param model 实体
     * @return 1 成功 | 0 失败
     */
    int delete(T model);

    /**
     * 主键删除
     * @param key 实体
     * @return 1 成功 | 0 失败
     */
    int deleteByPrimaryKey(Object key);

    int updateByPrimaryKey(T model);

    int updateByPrimaryKeySelective(T model);

    T selectByPrimaryKey(Object key);

    T selectOne(T model);

    T selectOneByExample(Object example);

    List<T> select(T model);

    List<T> selectByExample(Object example);

    List<T> selectAll();

    int selectCount(T model);

    int selectCountByExample(Object example);

    PageInfo<T> selectPage(T model, Integer page, Integer size);

    PageInfo<T> selectPageByExample(Object example, Integer page, Integer size);
}
