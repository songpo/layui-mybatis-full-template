package com.liusongpo.common;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 通用单表增删改查实现
 *
 * @author 刘松坡
 *
 * @param <T> 实体
 * @param <PK> 主键
 */
public class SimpleServiceImpl<T, PK> implements IBaseService<T, PK> {

    public Mapper<T> mapper;

    public SimpleServiceImpl(Mapper<T> mapper) {
        this.mapper = mapper;
    }

    @Override
    public int insert(T model) {
        return this.mapper.insert(model);
    }

    @Override
    public int insertSelective(T model) {
        return this.mapper.insertSelective(model);
    }

    @Override
    public int delete(T model) {
        return this.mapper.delete(model);
    }

    @Override
    public int deleteByPrimaryKey(Object key) {
        return this.mapper.deleteByPrimaryKey(key);
    }

    @Override
    public int updateByPrimaryKey(T model) {
        return this.mapper.updateByPrimaryKey(model);
    }

    @Override
    public int updateByPrimaryKeySelective(T model) {
        return this.mapper.updateByPrimaryKeySelective(model);
    }

    @Override
    public T selectByPrimaryKey(Object key) {
        return this.mapper.selectByPrimaryKey(key);
    }

    @Override
    public T selectOne(T model) {
        return this.mapper.selectOne(model);
    }

    @Override
    public T selectOneByExample(Object example) {
        return this.mapper.selectOneByExample(example);
    }

    @Override
    public List<T> select(T model) {
        return this.mapper.select(model);
    }

    @Override
    public List<T> selectByExample(Object example) {
        return this.mapper.selectByExample(example);
    }

    @Override
    public List<T> selectAll() {
        return this.mapper.selectAll();
    }

    @Override
    public int selectCount(T model) {
        return this.mapper.selectCount(model);
    }

    @Override
    public int selectCountByExample(Object example) {
        return this.mapper.selectCountByExample(example);
    }

    @Override
    public PageInfo<T> selectPage(T model, Integer page, Integer size) {
        if (null == page) {
            page = 1;
        }
        if (null == size) {
            size = 10;
        }
        PageHelper.startPage(page, size);
        List<T> list = null == model ? this.mapper.selectAll() : this.mapper.select(model);
        return new PageInfo<T>(list);
    }

    @Override
    public PageInfo<T> selectPageByExample(Object example, Integer page, Integer size) {
        if (null == page) {
            page = 1;
        }
        if (null == size) {
            size = 10;
        }
        PageHelper.startPage(page, size);
        List<T> list = example == null ? this.mapper.selectAll() : this.mapper.selectByExample(example);
        return new PageInfo<T>(list);
    }

}
