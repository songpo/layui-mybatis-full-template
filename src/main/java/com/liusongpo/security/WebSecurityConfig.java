package com.liusongpo.security;

import com.liusongpo.mapper.FakeAuthorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

/**
 * @author 刘松坡
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final MyUserDetailsServiceImpl myUserDetailsService;

    private final FakeAuthorityMapper fakeAuthorityMapper;

    @Autowired
    public WebSecurityConfig(MyUserDetailsServiceImpl myUserDetailsService, FakeAuthorityMapper fakeAuthorityMapper) {
        this.myUserDetailsService = myUserDetailsService;
        this.fakeAuthorityMapper = fakeAuthorityMapper;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(8);
    }

    @Bean
    public FilterSecurityInterceptor filterSecurityInterceptor() throws Exception {
        FilterSecurityInterceptor interceptor = new FilterSecurityInterceptor();
        interceptor.setAuthenticationManager(authenticationManager());
        interceptor.setAccessDecisionManager(new MyAccessDecisionManager());
        interceptor.setSecurityMetadataSource(new MySecurityMetadataSource(fakeAuthorityMapper));
        return interceptor;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/home", "/login", "/register", "/vendor/**", "/css/**", "/img/**", "/js/**", "/webjars/**").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .permitAll()
                .and()
            .logout()
                .logoutSuccessUrl("/login?logout")
                .permitAll()
                .and()
            .sessionManagement()
                .invalidSessionUrl("/login?sessionTimeout")
                .and()
            .addFilterAt(filterSecurityInterceptor(), FilterSecurityInterceptor.class)
            .exceptionHandling()
                .accessDeniedPage("/access-denied");
    }

    @Override
    protected UserDetailsService userDetailsService() {
        return myUserDetailsService;
    }

    //    @Override
//    public void init(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/vendor/**");
//        web.ignoring().antMatchers("/css/**");
//        web.ignoring().antMatchers("/img/**");
//        web.ignoring().antMatchers("/js/**");
//        web.ignoring().antMatchers("/webjars/**");
//    }
}
