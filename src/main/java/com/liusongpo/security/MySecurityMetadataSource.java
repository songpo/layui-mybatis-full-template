package com.liusongpo.security;

import com.liusongpo.entity.ResourceInfo;
import com.liusongpo.entity.RoleInfo;
import com.liusongpo.mapper.FakeAuthorityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author 刘松坡
 */
public class MySecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private static final Logger log = LoggerFactory.getLogger(MySecurityMetadataSource.class);

    private final FakeAuthorityMapper fakeAuthorityMapper;

    /**
     * 过期标识
     */
    private boolean expire = false;

    /**
     * 资源集合
     */
    private Map<String, Collection<ConfigAttribute>> kv = new HashMap<>();

    /**
     * 匹配规则
     */
    private RequestMatcher requestMatcher;

    /**
     * 规则标识
     */
    private String matcher = "ant";

    public MySecurityMetadataSource(FakeAuthorityMapper fakeAuthorityMapper) {
        this.fakeAuthorityMapper = fakeAuthorityMapper;
    }

    /**
     * 此方法用于检测当前用户访问的资源是否有相应的权限
     * @param object FilterInvocation 请求地址
     * @return 返回null的时候
     * @throws IllegalArgumentException
     */
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        log.debug("检测权限，参数 = {}", object);
        HttpServletRequest request = ((FilterInvocation) object).getRequest();
        // System.out.println("requestUrl is " + request.getRequestURI());

        // 检测是否刷新了资源
        if (isExpire()) {
            // 清空原本资源
            kv.clear();
            expire = false;
        }

        // 如果资源Map为空的时候则重新加载一次
        if (null == kv || kv.isEmpty()) {
            load();
        }

        // 检测请求与当前资源匹配的正确性
        for (String uri : kv.keySet()) {
            if ("ant".equals(matcher.toLowerCase())) {
                requestMatcher = new AntPathRequestMatcher(uri);
            }
            if ("regex".equals(matcher.toLowerCase())) {
                requestMatcher = new RegexRequestMatcher(uri, request.getMethod(), true);
            }
            if (requestMatcher.matches(request)) {
                return kv.get(uri);
            }
        }
        // 这里返回NULL则会对该URL进行放行
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        log.debug("加载权限信息");
        Set<ConfigAttribute> attributes = new HashSet<ConfigAttribute>();
        for (Map.Entry<String, Collection<ConfigAttribute>> entry : kv
                .entrySet()) {
            attributes.addAll(entry.getValue());
        }
        return attributes;
    }

    /**
     * 加载所有资源与权限的关系
     */
    public void load() {
        List<ResourceInfo> resources = this.fakeAuthorityMapper.selectResourceList();
        for (ResourceInfo resource : resources) {
            List<RoleInfo> roles = this.fakeAuthorityMapper.selectRoleListByResourceId(resource.getId());
            kv.put(resource.getContent(), list2Collection(roles));
        }
    }

    /**
     * 将List<Role>集合转换为框架需要的Collection<ConfigAttribute>集合
     *
     * @param roles
     * @return Collection<ConfigAttribute>
     */
    private Collection<ConfigAttribute> list2Collection(List<RoleInfo> roles) {
        List<ConfigAttribute> list = new ArrayList<>();
        for (RoleInfo role : roles) {
            list.add(new SecurityConfig(role.getName()));
        }
        return list;
    }

    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    public boolean isExpire() {
        return expire;
    }

    public void expireNow() {
        this.expire = true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
