package com.liusongpo.security;

import com.liusongpo.entity.UserInfo;
import com.liusongpo.mapper.FakeAuthorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsServiceImpl implements UserDetailsService {

    private final FakeAuthorityMapper fakeAuthorityMapper;

    @Autowired
    public MyUserDetailsServiceImpl(FakeAuthorityMapper fakeAuthorityMapper) {
        this.fakeAuthorityMapper = fakeAuthorityMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo userInfo = this.fakeAuthorityMapper.selectByUsername(username);
        if (null != userInfo) {
            LoginUser loginUser = new LoginUser();
            loginUser.setUserId(userInfo.getId());
            loginUser.setUsername(userInfo.getUsername());
            loginUser.setPassword(userInfo.getPassword());

            // 查询用户角色集合
            this.fakeAuthorityMapper.selectRoleListByUserId(userInfo.getId()).forEach(r -> loginUser.getRoles().add(r.getName()));
            return loginUser;
        }
        return null;
    }
}
