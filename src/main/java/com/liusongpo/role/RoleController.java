package com.liusongpo.role;

import com.github.pagehelper.PageInfo;
import com.liusongpo.entity.AuthorityInfo;
import com.liusongpo.entity.RoleInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/roles")
public class RoleController {

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/add")
    public ModelAndView add() {
        ModelAndView mav = new ModelAndView("views/role/add");
        List<AuthorityInfo> authorityInfoList = this.roleService.selectAuthorityList();
        mav.addObject("authorityList", authorityInfoList);
        return mav;
    }

    @PostMapping("/save")
    public RedirectView save(RoleInfo roleInfo, String authorities) {
        this.roleService.save(roleInfo, authorities);
        return new RedirectView("/roles/page");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable String id) {
        ModelAndView mav = new ModelAndView("views/role/edit");

        List<AuthorityInfo> authorityInfoList = this.roleService.selectAuthorityList();
        mav.addObject("authorityList", authorityInfoList);

        mav.addObject("item", this.roleService.selectById(id));
        return mav;
    }

    @GetMapping("/info/{id}")
    @ResponseBody
    public RoleInfo info(@PathVariable String id) {
        return this.roleService.selectById(id);
    }

    @PostMapping("/update")
    public RedirectView update(RoleInfo roleInfo, String authorities) {
        this.roleService.update(roleInfo, authorities);
        return new RedirectView("/roles/page");
    }

    @GetMapping("/delete/{id}")
    public RedirectView delete(@PathVariable String id) {
        this.roleService.deleteByPrimaryKey(id);
        return new RedirectView("/roles/page");
    }

    @GetMapping("/page")
    public ModelAndView page(String name, Integer page, Integer size) {
        ModelAndView mav = new ModelAndView("views/role/list");
        PageInfo<RoleInfo> pageInfo = this.roleService.selectPage(name, page, size);
        mav.addObject("pageInfo", pageInfo);
        mav.addObject("name", name);
        return mav;
    }
}
