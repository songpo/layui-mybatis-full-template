package com.liusongpo.role;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liusongpo.common.SimpleServiceImpl;
import com.liusongpo.entity.AuthorityInfo;
import com.liusongpo.entity.RoleAuthority;
import com.liusongpo.entity.RoleInfo;
import com.liusongpo.mapper.AuthorityInfoMapper;
import com.liusongpo.mapper.FakeAuthorityMapper;
import com.liusongpo.mapper.RoleAuthorityMapper;
import com.liusongpo.mapper.RoleInfoMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class RoleService extends SimpleServiceImpl<RoleInfo, String> {

    private final FakeAuthorityMapper fakeAuthorityMapper;

    private final RoleAuthorityMapper roleAuthorityMapper;

    private final AuthorityInfoMapper authorityInfoMapper;

    @Autowired
    public RoleService(RoleInfoMapper mapper, FakeAuthorityMapper fakeAuthorityMapper, RoleAuthorityMapper roleAuthorityMapper, AuthorityInfoMapper authorityInfoMapper) {
        super(mapper);
        this.fakeAuthorityMapper = fakeAuthorityMapper;
        this.roleAuthorityMapper = roleAuthorityMapper;
        this.authorityInfoMapper = authorityInfoMapper;
    }

    public void save(RoleInfo roleInfo, String authorities) {
        this.mapper.insertSelective(roleInfo);
        // 添加关系
        for (String authorityId : authorities.split(",")) {
            this.roleAuthorityMapper.insertSelective(new RoleAuthority(){{
                setRoleId(roleInfo.getId());
                setAuthorityId(authorityId);
            }});
        }
    }

    public void update(RoleInfo roleInfo, String authorities) {
        this.mapper.updateByPrimaryKeySelective(roleInfo);

        // 删除之前的关系
        this.roleAuthorityMapper.delete(new RoleAuthority(){{
            setRoleId(roleInfo.getId());
        }});

        // 添加关系
        for (String authorityId : authorities.split(",")) {
            this.roleAuthorityMapper.insertSelective(new RoleAuthority(){{
                setRoleId(roleInfo.getId());
                setAuthorityId(authorityId);
            }});
        }
    }

    public RoleInfo selectById(String id) {
        return this.fakeAuthorityMapper.selectRoleById(id);
    }

    public List<AuthorityInfo> selectAuthorityList() {
        return this.authorityInfoMapper.selectAll();
    }

    public PageInfo<RoleInfo> selectPage(String name, Integer page, Integer size) {
        Example example = new Example(RoleInfo.class);
        if (StringUtils.isNotBlank(name)) {
            example.createCriteria().andLike("name", "%" + name + "%");
        }
        if (null == page) {
            page = 1;
        }
        if (null == size) {
            size = 10;
        }
        PageHelper.startPage(page, size);
        List<RoleInfo> userInfoList = this.mapper.selectByExample(example);
        return new PageInfo<>(userInfoList);
    }
}
